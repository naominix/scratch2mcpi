## Scratch2MCPI(Scratch2MinecraftPi)

With Scratch2MCPI, you can control [Minecraft Pi Edition](http://pi.minecraft.net/) from [Scratch](http://scratch.mit.edu) on Raspberry Pi.

## Installation

Install Minecraft Pi Edition if you have not done. You can either install from http://pi.minecraft.net/?p=68 or use the following installation script.

```
# curl http://scratch2mcpi.github.io/mcpi.sh | sh
```

Install Scratch2MCPI

```
# curl http://scratch2mcpi.github.io/install.sh | sh
```

## Getting Started

1. Start Minecraft, click "Start Game", then "Create New". Wait until the new world is generated.
2. After the world is generated, double click Scratch2MCPI icon to start Scratch and Scratch2MCPI.
3. On Scratch, click Green flag, and run the script. "hello minecraft" should be displayed on Minecraft chat window.

## Requirements

[scratchpy](https://github.com/pilliq/scratchpy)

## Scratch2MCPITurtle(Scratch2MinecraftPi With [Minecraft Graphics Turtle](http://www.stuffaboutcode.com/2014/05/minecraft-graphics-turtle.html) + [Minecraft Stuff](https://github.com/martinohanlon/minecraft-stuff)

## Installation

Make file [install.sh](https://gist.github.com/naominix/976e6b451ff861460b86) in the /home/pi/ directory.

## Getting Started

1. Install Scratch2MCPI with MGT + MS by install.sh. 
2. Run the script. -> $ python scratch2mcpiturtle.py

## Requirements

[Minecraft Graphics Turtle](https://github.com/martinohanlon/minecraft-turtle)

[Minecraft Stuff](https://github.com/martinohanlon/minecraft-stuff)

